defmodule Day01 do
  @moduledoc false

  def calculate_frequency(string) do
    string
    |> String.split("\n")
    |> Enum.reduce(0, fn str, acc ->
      acc + convert_frequency(str)
    end)
  end

  def calculate_first_repeat_frequency(string) do
    history = %{0 => true}

    string
    |> String.split("\n")
    |> Enum.reduce_while(0, fn str, acc ->
      freq = acc + convert_frequency(str)
      if history[freq], do: {:halt, freq}, else: {:cont, freq}
    end)
  end

  defp convert_frequency("+" <> num) do
    {value, ""} = Integer.parse(num)

    value
  end

  defp convert_frequency("-" <> num) do
    {value, ""} = Integer.parse(num)

    -value
  end

  defp convert_frequency(""), do: 0
end
