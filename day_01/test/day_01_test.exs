defmodule Day01Test do
  use ExUnit.Case

  @test_string """
  +3
  -4
  +5
  """
  test "calculate frequency returns the correct frequency" do
    assert Day01.calculate_frequency(@test_string) == 4
  end

  test "calculate frequency with input file returns correctly" do
    input = File.read!("lib/input.txt")
    assert Day01.calculate_frequency(input) == 430
  end

  @test_string """
  +3
  +3
  +4
  -2
  -4
  """
  test "calculate_first_repeat_frequency returns the first frequency repeated" do
    assert Day01.calculate_first_repeat_frequency(@test_string) == 10
  end
end
