defmodule Day01P2 do
  def calc_repeat_frequency(file_stream) do
    Task.async(fn ->
      Process.put({__MODULE__, 0}, true)

      file_stream
      |> Stream.map(fn line ->
        {integer, _} = Integer.parse(line)
        integer
      end)
      |> Stream.cycle()
      |> Enum.reduce_while(0, fn x, acc ->
        new_freq = acc + x
        key = {__MODULE__, new_freq}

        if Process.get(key) do
          {:halt, new_freq}
        else
          Process.put(key, new_freq)
          {:cont, new_freq}
        end
      end)
    end)
    |> Task.await(:infinity)
  end
end

case System.argv() do
  ["--test"] ->
    ExUnit.start()

    defmodule Day01Test do
      use ExUnit.Case

      @input [
        "+3\n",
        "+3\n",
        "+4\n",
        "-2\n",
        "-4\n"
      ]
      test "calculate_first_repeat_frequency returns the first frequency repeated" do
        assert Day01P2.calc_repeat_frequency(@input) == 10
      end
    end

  [input_file] ->
    input_file
    |> File.stream!([], :line)
    |> Day01P2.calc_repeat_frequency()
    |> IO.puts()

  _ ->
    IO.puts(:stderr, "expected input file or --test")
    System.halt(1)
end
