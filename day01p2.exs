defmodule Day01P2 do
  def calc_repeat_frequency(file_stream) do
    file_stream
    |> Stream.map(fn line ->
      {integer, _} = Integer.parse(line)
      integer
    end)
    |> Stream.cycle()
    |> Enum.reduce_while({0, MapSet.new([0])}, fn x, {acc, history} ->
      new_freq = acc + x

      if new_freq in history do
        {:halt, new_freq}
      else
        {:cont, {new_freq, MapSet.put(history, new_freq)}}
      end
    end)
  end
end

case System.argv() do
  ["--test"] ->
    ExUnit.start()

    defmodule Day01Test do
      use ExUnit.Case

      @input [
        "+3\n",
        "+3\n",
        "+4\n",
        "-2\n",
        "-4\n"
      ]
      test "calculate_first_repeat_frequency returns the first frequency repeated" do
        assert Day01P2.calc_repeat_frequency(@input) == 10
      end
    end

  [input_file] ->
    input_file
    |> File.stream!([], :line)
    |> Day01P2.calc_repeat_frequency()
    |> IO.puts()

  _ ->
    IO.puts(:stderr, "expected input file or --test")
    System.halt(1)
end
