defmodule Day01 do
  def calc_frequency(file_stream) do
    file_stream
    |> Stream.map(fn line ->
      {integer, _} = Integer.parse(line)
      integer
    end)
    |> Enum.sum()
  end
end

case System.argv() do
  ["--test"] ->
    ExUnit.start()

    defmodule Day01Test do
      use ExUnit.Case

      import Day01

      @test_string """
      +3
      -4
      +5
      """
      test "calculate frequency retargsurns the correct frequency" do
        {:ok, io} = StringIO.open(@test_string)
        assert calc_frequency(IO.stream(io, :line)) == 4
      end
    end

  [input_file] ->
    input_file
    |> File.stream!([], :line)
    |> Day01.calc_frequency()
    |> IO.puts()

  _ ->
    IO.puts(:stderr, "expected input file or --test")
    System.halt(1)
end
