defmodule Day02 do
  def closest(list) when is_list(list) do
    list
    |> Enum.map(&String.to_charlist/1)
    |> closest_charlist()
  end

  def closest_charlist([head | tail]) do
    Enum.find_value(tail, &one_char_difference?(&1, head)) || closest_charlist(tail)
  end

  defp one_char_difference?(string1, string2) do
    string1
    |> Enum.zip(string2)
    |> Enum.split_with(fn {cp1, cp2} -> cp1 == cp2 end)
    |> case do
      {tuples_of_codepoints, [_]} ->
        tuples_of_codepoints
        |> Enum.map(fn {cp1, _} -> cp1 end)
        |> List.to_string()
      _ -> nil
    end
  end

  def checksum(list) when is_list(list) do
    {twices, thrices} =
      Enum.reduce(list, {0, 0}, fn id, {total2, total3} ->
        {twice, thrice} = id |> count_characters() |> get_twice_and_thrice()
        {twice + total2, thrice + total3}
      end)

    twices * thrices
  end

  def count_characters(string) when is_binary(string) do
    count_characters(string, %{})
  end

  defp count_characters(<<codepoint::utf8, tail::binary>>, acc) do
    acc = Map.update(acc, codepoint, 1, &(&1 + 1))
    count_characters(tail, acc)
  end

  defp count_characters(<<>>, acc), do: acc

  defp get_twice_and_thrice(characters) when is_map(characters) do
    Enum.reduce(characters, {0, 0}, fn
      {_codepoint, 2}, {_twice, thrice} -> {1, thrice}
      {_codepoint, 3}, {twice, _thrice} -> {twice, 1}
      _, acc -> acc
    end)
  end
end
