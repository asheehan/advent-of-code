defmodule Day02P2 do
  def calc(input) do
    input
    |> Stream.map(fn line ->
      String.codepoints(line)
    end)
    |> Enum.reduce(fn codepoints ->
      count2s(codepoints)
    end)
  end

  defp count2s(codepoints) do
    codepoints
  end
end

case System.argv() do
  ["--test"] ->
    ExUnit.start()

    defmodule Day02Test do
      use ExUnit.Case

      @input [
        "abcdef\n",
        "bababc\n",
        "abbcde\n",
        "abcccd\n",
        "aabcdd\n",
        "abcdee\n",
        "ababab\n"
      ]
      test "calculate_first_repeat_frequency returns the first frequency repeated" do
        assert Day02P2.calc(@input) == 12
      end
    end

  [input_file] ->
    input_file
    |> File.stream!([], :line)
    |> Day02P2.calc()
    |> IO.puts()

  _ ->
    IO.puts(:stderr, "expected input file or --test")
    System.halt(1)
end
