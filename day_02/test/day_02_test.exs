defmodule Day02Test do
  use ExUnit.Case

  test "count_characters" do
    assert Day02.count_characters("aabbcc") == %{
             ?a => 2,
             ?b => 2,
             ?c => 2
           }

    assert Day02.count_characters("ébbccé") == %{
             ?é => 2,
             ?b => 2,
             ?c => 2
           }
  end

  test "checksum" do
    assert Day02.checksum([
             "abcdef",
             "bababc",
             "abbcde",
             "abcccd",
             "aabcdd",
             "abcdee",
             "ababab"
           ]) == 12
  end

  test "closest" do
    assert Day02.closest([
             "abcde",
             "fghij",
             "klmno",
             "pqrst",
             "fguij",
             "axcye",
             "wvxyz"
           ]) == "fgij"
  end
end
